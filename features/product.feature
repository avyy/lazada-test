Feature: Product info should be the same as in cart

  Scenario Outline:
    Given I open random product page in random category with <browser>
    When I add product to my cart
    Then Product title and price should be the same

    Examples:
      | browser |
      | chrome  |
      | firefox |
      | safari  |

