Given "I open home page with $browser" do |browser|
  browser = Browser.open(browser.to_sym)
  @home = Page::Home.new(browser)
end

And "I add two random products to cart" do
  product_page = @home.open_random_product(2, 3)
  cart = product_page.add_to_cart
  home = cart.go_home
  product_page = home.open_random_product(2, 2)
  @cart = product_page.add_to_cart
end

When "I change qty of any product in cart" do
  @cart.get_product_info
  @old_sub = @cart.subtotal
  @cart.change_qty_of_random_item
end

Then "Subtotal should change" do
  @cart.get_product_info
  sub = @cart.subtotal
  assert unless @old_sub == sub
end
