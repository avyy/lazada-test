Given "I visit main page with $browser" do |browser|
  browser = Browser.open(browser.to_sym)
  @home = Page::Home.new(browser)
end

And "I click Signup element" do
  @signup_page = @home.signup
end

And "I fill required fields except of passwd confirm" do
  @signup_page.fill_fields
end

When "I submit form" do
  @signup_page.submit
end

Then "I receive an error message" do
  assert unless @signup_page.check_error
end
