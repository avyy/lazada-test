Given "I open random product page in random category with $browser" do |browser|
  browser = Browser.open(browser.to_sym)
  @home = Page::Home.new(browser)
  @product_page = @home.open_random_product
end

When "I add product to my cart" do
  @cart_page = @product_page.add_to_cart
end

Then "Product title and price should be the same" do
  @cart_page.get_product_info
  assert unless @product_page.product_title == @cart_page.product_title
  assert unless @product_page.product_price == @cart_page.product_price
end
