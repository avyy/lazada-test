Feature: Subtotal price in cart should change when qty is cahnged

  Scenario Outline:
    Given I open home page with <browser>
    And I add two random products to cart
    When I change qty of any product in cart
    Then Subtotal should change

    Examples:
      | browser |
      | chrome  |
      | firefox |
      | safari  |

