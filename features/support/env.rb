require 'selenium-webdriver'

module Page
  class Home
    attr_reader :driver

    def initialize(driver)
      @driver = driver
      driver.navigate.to 'http://lazada.vn'
      # fail if it is not a home page
    end

    def signup
      li = driver.find_element :xpath, "//li[contains(@class, 'header-authentication-popup') and contains(@data-authen, '1')]"
      li.click
      SignupPopup.new driver
    end

    def open_random_product(category = nil, number = nil)
      category ||= rand(1..7)
      number   ||= rand(1..4)

      wait = Selenium::WebDriver::Wait.new(:timeout => 10)
      li = driver.find_element :xpath, ".//ul[contains(@class, 'sidebar__list')]/li[#{category}]"
      li.click

      wait.until { driver.find_element :xpath, ".//div[contains(@class, 'product_list')]/a[2]" }
      a = driver.find_element :xpath, ".//div[contains(@class, 'product_list')]/a[#{number}]"
      a.click
      ProductInfo.new(driver)
    end
  end

  class SignupPopup
    attr_accessor :name, :email, :password
    attr_reader :driver

    DEFAULT = { name: 'qwerty', email: 'qwerty@gmail.com', password: 'qwerty007' }
    
    def initialize(driver)
      @driver = driver
      @name, @email, @password = DEFAULT[:name], DEFAULT[:email], DEFAULT[:password]
    end

    def fill_fields
      fields =  { name: [:id, 'RegistrationForm_first_name'], 
                  email: [:id, :RegistrationForm_email], 
                  password: [:id, 'RegistrationForm_password'] }

      wait = Selenium::WebDriver::Wait.new(:timeout => 10)
      wait.until do
        driver.find_element(:xpath, "//input[contains(@id, 'RegistrationForm_password2')]/following::span")
        driver.find_element(*fields[:name])
        driver.find_element(*fields[:email])
        driver.find_element(*fields[:password])
      end
      field_name = driver.find_element(*fields[:name])
      field_email = driver.find_element(*fields[:email])
      pass = driver.find_element(*fields[:password])
      field_name.send_keys name
      field_email.send_keys email
      pass.send_keys password
    end

    def submit
      button = driver.find_element(:id, 'send')
      button.click
    end

    def check_error
      error = driver.find_element(:xpath, "//input[contains(@id, 'RegistrationForm_password2')]/following::span")
      error.displayed?
    end
  end

  class ProductInfo
    attr_reader :driver, :product_price, :product_title
    def initialize(driver)
      @driver = driver
      @wait = Selenium::WebDriver::Wait.new(:timeout => 10)
      @wait.until { driver.find_element(:css, 'span#special_price_box') }
      @product_price = driver.find_element(:css, 'span#special_price_box').text
      @product_title = driver.find_element(:css, 'h1#prod_title').text
    end
    
    def add_to_cart
      driver.find_element(:css, 'button#AddToCart').click
      CartPopup.new(driver)
    end
  end

  class CartPopup
    attr_reader :driver, :product_title, :product_price, :subtotal

    def initialize(driver)
      @driver = driver
      wait = Selenium::WebDriver::Wait.new(:timeout => 10)
      wait.until { driver.find_element(:css, ".productdescription") }
    end

    def get_product_info
      @product_title = driver.find_element(:css, ".productdescription").text
      @product_price = driver.find_element(:xpath, ".//td[contains(@class, 'price')][1]/span[1]").text
      @subtotal = driver.find_element(:css, ".sub .lastcolumn div").text
    end

    def go_home
      Home.new(driver)
    end

    def change_qty_of_random_item
      options = driver.find_elements :css, "select.cart-product-actions-select option"
      options.last.click
    end

  end
end

module Browser
  # Standalone server must be started, and `name'-selenium driver should be initialized
  @@browsers = []

  def self.open(name)
    driver = Selenium::WebDriver.for(:remote, :desired_capabilities => name)
    @@browsers << driver
    driver
  end

  def self.close_browsers
    @@browsers.each { |b| b.close }
    @@browsers = []
  end
end

World(Page, Browser)

After do |_|
  Browser.close_browsers
end

