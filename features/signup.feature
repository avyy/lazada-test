Feature: Show correct error message on Signup
  Show error message if password confirmation field is blank

  Scenario Outline:
    Given I visit main page with <browser>
    And I click Signup element
    And I fill required fields except of passwd confirm
    When I submit form
    Then I receive an error message

    Examples:
      | browser |
      | chrome  |
      | firefox |
      | safari  |

