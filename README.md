# Lazada Test

### Environment

  - MacOS
  - JRE/JDK
  - ruby 2.1.5p273 (also [rubygems], [bundler])
  - Last Chrome, Firefox, Safari
  - `brew install selenium-server-standalone` (it means that [homebrew] should be installed)
  - [Chrome Driver]
 
### How to launch tests

```sh
$ git clone https://avyy@bitbucket.org/avyy/lazada-test.git
$ cd lazada-test
$ bundle # to install cucumber and selenium-webdriver
$ selenium-server -Dwebdriver.chrome.driver=/path/to/downloaded/chromedriver -p4444
$ bundle exec cucumber -fpretty
```


### Problems
100% tests are passed only with chrome, because there are some problems with Safari and Firefox webdriver-extensions (they are breaking sometimes on MacOS).

### PS
It would be more powerful to use tools like capybara that gives new abstraction level for selenium, but i thought i must use official selenium-webdriver (?). 

[Chrome Driver]:http://chromedriver.storage.googleapis.com/index.html?path=2.12/
[homebrew]:http://brew.sh/
[rubygems]:http://rubygems.org
[bundler]:http://bundler.io